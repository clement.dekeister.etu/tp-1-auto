FROM maven:3.8.1-openjdk-11

WORKDIR /app

COPY pom.xml .
COPY src ./src
RUN mvn install -Dmaven.test.skip=true
EXPOSE 8080
CMD ["java", "-jar", "target/GetThingsDone-app-1.0-SNAPSHOT.jar"]